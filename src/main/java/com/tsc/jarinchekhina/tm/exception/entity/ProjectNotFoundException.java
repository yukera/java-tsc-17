package com.tsc.jarinchekhina.tm.exception.entity;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
