package com.tsc.jarinchekhina.tm.command.system;

import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.Collection;

public class CommandsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-сmd";
    }

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "display list of commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<String> commands = serviceLocator.getCommandService().getCommandNames();
        for (final String command : commands) System.out.println(command);
    }

    private void showCommandValue(final String value) {
        if (DataUtil.isEmpty(value)) return;
        System.out.println(value);
    }

}
