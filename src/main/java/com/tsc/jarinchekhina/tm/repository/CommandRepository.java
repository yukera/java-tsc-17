package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getCommandNames() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command: commands.values()) {
            final String name = command.name();
            if (DataUtil.isEmpty(name)) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public Collection<String> getCommandArgs() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command: arguments.values()) {
            final String name = command.name();
            if (DataUtil.isEmpty(name)) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String name) {
        return arguments.get(name);
    }

    @Override
    public void add(AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (!DataUtil.isEmpty(arg)) arguments.put(arg, command);
        if (!DataUtil.isEmpty(name)) commands.put(name, command);
    }

}
