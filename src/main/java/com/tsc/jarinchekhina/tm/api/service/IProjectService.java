package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void add(Project project);

    void remove(Project project);

    void clear();

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project create(String name) throws EmptyNameException;

    Project create(String name, String description) throws AbstractException;

    Project findOneById(String id) throws EmptyIdException;

    Project findOneByIndex(Integer index) throws IndexIncorrectException;

    Project findOneByName(String name) throws EmptyNameException;

    Project removeOneById(String id) throws EmptyIdException;

    Project removeOneByIndex(Integer index) throws IndexIncorrectException;

    Project removeOneByName(String name) throws EmptyNameException;

    Project updateProjectById(String id, String name, String description) throws AbstractException;

    Project updateProjectByIndex(Integer index, String name, String description) throws AbstractException;

    Project startProjectById(String id) throws AbstractException;

    Project startProjectByIndex(Integer index) throws AbstractException;

    Project startProjectByName(String name) throws AbstractException;

    Project finishProjectById(String id) throws AbstractException;

    Project finishProjectByIndex(Integer index) throws AbstractException;

    Project finishProjectByName(String name) throws AbstractException;

    Project changeProjectStatusById(String id, Status status) throws AbstractException;

    Project changeProjectStatusByIndex(Integer index, Status status) throws AbstractException;

    Project changeProjectStatusByName(String name, Status status) throws AbstractException;

}
