package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.api.service.*;
import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.command.project.*;
import com.tsc.jarinchekhina.tm.command.system.*;
import com.tsc.jarinchekhina.tm.command.task.*;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.system.UnknownArgumentException;
import com.tsc.jarinchekhina.tm.exception.system.UnknownCommandException;
import com.tsc.jarinchekhina.tm.repository.CommandRepository;
import com.tsc.jarinchekhina.tm.repository.ProjectRepository;
import com.tsc.jarinchekhina.tm.repository.TaskRepository;
import com.tsc.jarinchekhina.tm.service.*;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator{

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    {
        registry(new CommandsCommand());
        registry(new ArgumentsCommand());
        registry(new AboutCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new ExitCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectListCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectStatusChangeByIdCommand());
        registry(new ProjectStatusChangeByIndexCommand());
        registry(new ProjectStatusChangeByNameCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskListCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskStatusChangeByIdCommand());
        registry(new TaskStatusChangeByIndexCommand());
        registry(new TaskStatusChangeByNameCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskBindByProjectIdCommand());
        registry(new TaskUnbindByProjectIdCommand());
        registry(new TaskShowByProjectIdCommand());
    }

    private void initData() throws EmptyNameException {
        projectService.create("Project 1").setStatus(Status.IN_PROGRESS);
        projectService.create("PROJECT").setStatus(Status.COMPLETED);
        projectService.create("ASDSAD1");
        taskService.create("Task 1");
        taskService.create("TASK").setStatus(Status.IN_PROGRESS);
        taskService.create("adskJN").setStatus(Status.COMPLETED);
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void parseArgs(final String[] args) throws AbstractException {
        if (DataUtil.isEmpty(args)) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    private void parseArg(final String arg) throws AbstractException {
        if (DataUtil.isEmpty(arg)) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) throw new UnknownArgumentException(arg);
        command.execute();
    }

    private void parseCommand(final String cmd) throws AbstractException {
        if (DataUtil.isEmpty(cmd)) throw new UnknownCommandException(cmd);
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    public void start(final String[] args) throws AbstractException {
        logService.info("*** Welcome to Task Manager ***");
        parseArgs(args);
        initData();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (Exception e) {
                logService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

}
